﻿using UnityEngine;

public class CoinController : MonoBehaviour {

    private CoinHud coinHud;

    void Awake() {

        coinHud = GameObject.Find("CoinHud").GetComponent<CoinHud>();

    }

    void OnTriggerEnter2D(Collider2D other) {

        coinHud.PickUpCoin();

        Destroy(gameObject);

    }

}
