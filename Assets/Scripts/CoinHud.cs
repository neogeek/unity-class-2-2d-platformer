﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CoinHud : MonoBehaviour {

    public int coinCount = 0;

    public Text coinText;
    public AudioSource coinPickup;
    public AudioSource winSong;

    private readonly int winCount = 10;

    public void PickUpCoin() {

        coinCount += 1;

        coinPickup.Play();

        coinText.text = coinCount.ToString();

        if (coinCount >= 10) {

            winSong.Play();

            // SceneManager.LoadScene("Main");

        }

    }

}
