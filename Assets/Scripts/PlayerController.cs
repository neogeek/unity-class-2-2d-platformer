﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

    public LayerMask groundLayerMask;
    public Transform groundChecker;

    private readonly float maxHorizontalSpeed = 10.0f;
    private readonly float jumpForce = 700.0f;

    private readonly float groundedRadius = 0.1f;
    private readonly float landingRadius = 0.3f;

    private bool landing = false;
    private bool grounded = false;

    private Animator animator;
    private Rigidbody2D rb;

    private bool faceRight = true;
    private float horizontalMovement = 0;

    void Awake() {

        animator = gameObject.GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody2D>();

    }

    void Start() {

    }

    void Update() {

        horizontalMovement = Input.GetAxis("Horizontal");

        animator.SetFloat("horizontalSpeed", Mathf.Abs(horizontalMovement));

        rb.velocity = new Vector2(horizontalMovement * maxHorizontalSpeed, rb.velocity.y);

        if (horizontalMovement > 0 && !faceRight) Flip();
        else if (horizontalMovement < 0 && faceRight) Flip();

        if (grounded && Input.GetButtonDown("Jump")) {

            animator.SetBool("grounded", false);
            animator.SetBool("landing", false);

            rb.AddForce(new Vector2(0, jumpForce));

        }

    }

    void FixedUpdate() {

        grounded = Physics2D.OverlapCircle(groundChecker.position, groundedRadius, groundLayerMask);
        animator.SetBool("grounded", grounded);

        landing = Physics2D.OverlapCircle(groundChecker.position, landingRadius, groundLayerMask);
        animator.SetBool("landing", landing);

        animator.SetFloat("verticalSpeed", rb.velocity.y);

    }

    void Flip() {

        faceRight = !faceRight;

        Vector3 scale = gameObject.transform.localScale;
        scale.x *= -1;
        gameObject.transform.localScale = scale;

        // rb.velocity = new Vector2(0, rb.velocity.y);

    }

}
